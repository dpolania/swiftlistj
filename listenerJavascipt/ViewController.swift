//
//  ViewController.swift
//  listenerJavascipt
//
//  Created by Macbook  on 24/03/20.
//  Copyright © 2020 Macbook . All rights reserved.
//

import UIKit
import WebKit
import Foundation

class WebViewController: UIViewController , UIWebViewDelegate  {

    @IBOutlet weak var viewWebPage: UIView!
    var webView: WKWebView!

    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createrWebView(view : viewWebPage)
        configureWebView(url: "https://pruebasservice.000webhostapp.com/widget.php")
    }
    /**
     They create the web view which will contain the page to display, it uses a view and inserts the child view into it using its dimensions
        - Parameter view: view where webview will be inserted
     */
    fileprivate func createrWebView(view : UIView) {
        
        let contentController = WKUserContentController()
        contentController.add(self, name: "sendData")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        self.webView = WKWebView( frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), configuration: config)
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        WKWebView.clean()
        view.addSubview(self.webView)
    }
    /**
     Configure the web that you want to load in the webview
        - Parameter url: linked to load page
     */
    fileprivate func configureWebView(url linked : String) {
        let url = URL(string: linked)!
        let request = URLRequest(url: url)
        webView.navigationDelegate = self
        webView.load(request)
    }
}



extension WebViewController : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}

extension WebViewController : WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print(message.body)
        let values = (message.body as! [String:String]).values
        let result = values.joined(separator: " - ")
        
        let alertController = UIAlertController(title: "Result", message:
            "values : \(result)", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))

        self.present(alertController, animated: true, completion: nil)
        for item in self.view.subviews{
            if item == view{
                view.removeFromSuperview()
            }
        }
        createrWebView(view : viewWebPage)
        configureWebView(url: "https://pruebasservice.000webhostapp.com/widget.php")
    }
}


extension WKWebView {
    class func clean() {
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("[WebCacheCleaner] All cookies deleted")
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
                print("[WebCacheCleaner] Record \(record) deleted")
            }
        }
    }
}
